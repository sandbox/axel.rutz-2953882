<?php

namespace Drupal\download_file;

use Drupal\file\Entity\File;

/**
 * {@inheritdoc}
 */
class DownloadHeaders implements DownloadHeadersInterface {

  /**
   * {@inheritdoc}
   */
  public function build(File $file) {
    return [
      'Content-Type' => 'force-download',
      'Content-Disposition' => 'attachment; filename="' . $file->getFilename() . '"',
      'Content-Length' => $file->getSize(),
      'Content-Transfer-Encoding' => 'binary',
      'Pragma' => 'no-cache',
      'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
      'Expires' => '0',
      'Accept-Ranges' => 'bytes',
    ];
  }

}
