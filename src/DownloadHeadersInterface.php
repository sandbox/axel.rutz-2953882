<?php

namespace Drupal\download_file;

use Drupal\file\Entity\File;

/**
 * Build download headers.
 */
interface DownloadHeadersInterface {

  /**
   * Build download headers.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file.
   *
   * @return array
   *   The headers.
   */
  public function build(File $file);

}
