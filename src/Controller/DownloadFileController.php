<?php

namespace Drupal\download_file\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\download_file\DownloadHeaders;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Drupal\file\Entity\File;

/**
 * Class DownloadFileController.
 *
 * @package Drupal\download_file\Controller
 */
class DownloadFileController extends ControllerBase {

  /**
   * The Download headers service.
   *
   * @var \Drupal\download_file\DownloadHeaders
   */
  protected $downloadHeadersService;

  /**
   * DownloadFileController constructor.
   *
   * @param \Drupal\download_file\DownloadHeaders $downloadHeadersService
   *   The Download headers service.
   */
  public function __construct(DownloadHeaders $downloadHeadersService) {
    $this->downloadHeadersService = $downloadHeadersService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('download_file.headers')
    );
  }

  /**
   * Check access.
   *
   * We don't have logic to grant user's own temporary files (between uploading
   * and saving) until there's a real need for it.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access.
   *
   * @see \Drupal\system\FileDownloadController::download
   * @see \file_file_download
   */
  public function access(File $file) {
    return AccessResult::allowedIf($file->isPermanent() && $file->access('download'));
  }

  /**
   * Send response.
   *
   * @param \Drupal\file\Entity\File $file
   *   The File. Already checked for download access in routing.yml.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The Response.
   */
  public function download(File $file) {

    $default_headers = file_get_content_headers($file);
    $download_headers = $this->downloadHeadersService->build($file);

    $headers = array_merge($default_headers, $download_headers);
    return new BinaryFileResponse($file->getFileUri(), 200, $headers);
  }

}
