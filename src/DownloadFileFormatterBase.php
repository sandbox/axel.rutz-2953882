<?php

namespace Drupal\download_file;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Base class for 'download_file' formatter.
 */
abstract class DownloadFileFormatterBase extends FileFormatterBase {

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    /** @var \Drupal\file\Entity\File $file */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $item = $file->_referringItem;
      $id = $file->id();
      $link_text = $this->makeLinktext($item, $file);
      $elements[$delta] = [
        '#link_text' => $link_text,
        '#file' => $id,
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];

      if (isset($item->_attributes)) {
        $elements[$delta] += ['#attributes' => []];
        $elements[$delta]['#attributes'] += $item->_attributes;
        unset($item->_attributes);
      }

      $this->finishElement($elements[$delta]);
    }

    return $elements;
  }

  /**
   * Adds implementation specific parts.
   *
   * @param array $element
   *   The field item element, by reference.
   *
   * @see ::viewElements
   */
  abstract protected function finishElement(array &$element);

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'linktext_type' => 'filename',
      'linktext' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['linktext_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Link text type'),
      '#default_value' => $this->getSetting('linktext_type'),
      '#options' => $this->getLinktextOptions(),
    ];
    // @todo Ajaxify: https://www.drupal.org/docs/8/creating-custom-modules/create-a-custom-field-formatter
    // https://www.drupal.org/project/drupal/issues/2927947
    // @see views_ui_add_ajax_trigger
    $element['linktext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#default_value' => $this->getSetting('linktext'),
      '#access' => $this->getSetting('linktext_type') === 'text',
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $type = $this->getSetting('linktext_type');
    $typeLabel = $this->getLinktextOptions()[$type];
    $summary[] = $this->t('Link text type: @type', ['@type' => $typeLabel]);
    if ($type === 'text') {
      $summary[] = $this->t('Link text: @text', ['@text' => $this->getSetting('linktext')]);
    }
    return $summary;
  }

  /**
   * Generate the link text.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item.
   * @param \Drupal\file\Entity\File $file
   *   The file.
   *
   * @return string
   *   The link text.
   */
  protected function makeLinktext(FieldItemInterface $item, File $file) {
    switch ($this->getSetting('linktext_type')) {
      case 'filename':
        return $file->getFilename();

      case 'text':
        return $this->getSetting('linktext') ?: $this->t('Download');
    }
  }

  /**
   * Get link text options.
   *
   * @return array
   *   The options for use in a form.
   */
  protected function getLinktextOptions() {
    return [
      'filename' => $this->t('Filename'),
      'text' => $this->t('Fixed text'),
    ];
  }

}
