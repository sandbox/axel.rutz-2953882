<?php

namespace Drupal\download_file\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\download_file\DownloadFileFormatterBase;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'download_file' Formatter.
 *
 * @FieldFormatter(
 *   id = "download_file" ,
 *   label = @Translation("Download File"),
 *   field_types = {
 *    "file", "image"
 *   }
 * )
 */
class DownloadFileFormatter extends DownloadFileFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function finishElement(array &$element) {
    $element += [
      '#theme' => 'download_file_link',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function makeLinktext(FieldItemInterface $item, File $file) {
    // @fixme Check if this is a file or a image.
    switch ($this->getSetting('linktext_type')) {
      case 'description':
        return !empty($item->description) ? $item->description : $file->getFilename();
    }
    return parent::makeLinktext($item, $file);
  }

  /**
   * {@inheritdoc}
   */
  protected function getLinktextOptions() {
    return [
      'description' => $this->t('Description'),
    ] + parent::getLinktextOptions();
  }

}
