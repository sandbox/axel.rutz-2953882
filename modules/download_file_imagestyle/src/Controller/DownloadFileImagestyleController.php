<?php

namespace Drupal\download_file_imagestyle\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\download_file\DownloadHeadersInterface;
use Drupal\image\Controller\ImageStyleDownloadController;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DownloadFileImagestyleController.
 *
 * @package Drupal\download_file_imagestyle\Controller
 *
 * @see \Drupal\image\Controller\ImageStyleDownloadController::deliver
 */
class DownloadFileImagestyleController extends ControllerBase {

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * The Download headers service.
   *
   * @var \Drupal\download_file\DownloadHeadersInterface
   */
  protected $downloadHeadersService;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * DownloadFileController constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param \Drupal\download_file\DownloadHeaders|\Drupal\download_file\DownloadHeadersInterface $downloadHeadersService
   *   The Download headers service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   */
  public function __construct(ContainerInterface $container, DownloadHeadersInterface $downloadHeadersService, FileSystemInterface $fileSystem) {
    $this->container = $container;
    $this->downloadHeadersService = $downloadHeadersService;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('download_file.headers'),
      $container->get('file_system')
    );
  }

  /**
   * Check access.
   *
   * We don't have logic to grant user's own temporary files (between uploading
   * and saving) until there's a real need for it.
   *
   * @param \Drupal\file\Entity\File $file
   *   File.
   * @param \Drupal\image\Entity\ImageStyle $image_style
   *   Image style.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access.
   */
  public function access(File $file, ImageStyle $image_style) {
    $access = AccessResult::allowedIf($file->isPermanent() && $file->access('download'));
    return $access;
  }

  /**
   * Download image style.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   * @param \Drupal\file\Entity\File $file
   *   File.
   * @param \Drupal\image\Entity\ImageStyle $image_style
   *   Image style.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   Response.
   *
   * @see \Drupal\image\Plugin\Field\FieldFormatter\ImageUrlFormatter::viewElements
   * @see \Drupal\image\Controller\ImageStyleDownloadController::deliver
   */
  public function download(Request $request, File $file, ImageStyle $image_style) {

    // This does not have a service name.
    $controller = ImageStyleDownloadController::create($this->container);
    $image_uri = $file->getFileUri();
    $scheme = $this->fileSystem->uriScheme($image_uri);
    $url = $image_style->buildUrl($image_uri);
    $pathWithToken = file_url_transform_relative($url);
    $parsed = UrlHelper::parse($pathWithToken);
    $token = $parsed['query'][IMAGE_DERIVATIVE_TOKEN];
    $fullPath = $parsed['path'];
    // We have to fake the request with file and token parameters. Sigh.
    $fakeRequest = new Request([
      IMAGE_DERIVATIVE_TOKEN => $token,
    ]);
    /** @var \Drupal\image\PathProcessor\PathProcessorImageStyles $processor */
    $processor = \Drupal::service('path_processor.image_styles');
    $processor->processInbound($fullPath, $fakeRequest);

    $response = $controller->deliver($fakeRequest, $scheme, $image_style);

    $response->headers->add($this->downloadHeadersService->build($file));
    return $response;
  }

}
