<?php

namespace Drupal\download_file_imagestyle\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\download_file\DownloadFileFormatterBase;

/**
 * Plugin implementation of the 'download_file' Formatter.
 *
 * @FieldFormatter(
 *   id = "download_file_imagestyle" ,
 *   label = @Translation("Download imagestyle"),
 *   field_types = {
 *    "image"
 *   }
 * )
 */
class DownloadFileImagestyleFormatter extends DownloadFileFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function finishElement(array &$element) {
    $element += [
      '#theme' => 'download_file_imagestyle_link',
      '#imagestyle' => $this->getSetting('imagestyle'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'imagestyle' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['imagestyle'] = [
      '#type' => 'select',
      '#title' => $this->t('Imagestyle'),
      '#default_value' => $this->getSetting('imagestyle'),
      '#options' => image_style_options(TRUE),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $imageStyleOptions = image_style_options(TRUE);
    $style = $imageStyleOptions[$this->getSetting('imagestyle')];
    $summary[] = $this->t('Style: @style', ['@style' => $style]);
    return $summary;
  }

  // @todo Add title and alt as link text options.
}
